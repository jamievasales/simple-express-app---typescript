import express from "express";
import winston from "winston";

const app = express();
const port = 8080; // default port to listen
const logger = winston.createLogger({
    transports: [
        new winston.transports.Console()
    ]
});

app.listen(port, () => {
    logger.info("Logged");
});

function loggerMiddleware(request: express.Request, response: express.Response, next) {
  logger.info(`${request.method} ${request.path}`);
  next();
}

app.use(loggerMiddleware);

app.get("/", (request, response) => {
  response.send("Hello world!");
});
